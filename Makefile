all: src/*.java
	javac -sourcepath src -d bin src/Main.java

run: bin/Main.class
	java -ea -cp bin Main "Lab1.map" 20 10 20

clean:
	rm -rf bin/*
