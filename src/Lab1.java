import java.util.concurrent.Semaphore;

import TSim.*;

enum HeadDirection {
  UpToDown,
  DownToUp,
}

enum SwitchState {
  Up,
  Down,
}

class Constant {
  public static final int TRACK_COUNT = 8;
  public static final int DONT_CARE = -1;
}

class Semaphores {
  public Semaphore[] tracks = new Semaphore[Constant.TRACK_COUNT];
  public Semaphore cross = new Semaphore(1);

  public Semaphores() {
    for (int i = 0; i < Constant.TRACK_COUNT; ++i) {
      this.tracks[i] = new Semaphore(1);
    }
  }
}

class Train implements Runnable {
  public Semaphores semaphores;
  public int id;
  public int maxSpeed;
  public int currentAt;
  public int goingTo;
  public TSimInterface tsi;

  public Train(int speed, int id, Semaphores semaphores) {
    this.maxSpeed = speed;
    this.semaphores = semaphores;
    this.id = id;
    this.tsi = TSimInterface.getInstance();
    if (id == 1) {
      currentAt = 0;
      goingTo = 2;
      try {
        this.semaphores.tracks[0].acquire();
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else {
      currentAt = 6;
      goingTo = 5;
      try {
        this.semaphores.tracks[6].acquire();
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  public void stop() {
    try {
      tsi.setSpeed(this.id, 0);
    } catch (CommandException e) {
      e.printStackTrace();
    }
  }

  public void start() {
    try {
      if (id == 1) {
        if (currentAt < goingTo) {
          tsi.setSpeed(this.id, this.maxSpeed);
        } else {
          tsi.setSpeed(this.id, -this.maxSpeed);
        }
      } else {
        if (currentAt < goingTo) {
          tsi.setSpeed(this.id, -this.maxSpeed);
        } else {
          tsi.setSpeed(this.id, this.maxSpeed);
        }
      }
    } catch (CommandException e) {
      e.printStackTrace();
    }
  }

  public int[] sensorIdToPosition(int sensorId) {
    int[][] positions = {
        { 13, 3 },
        { 13, 5 },
        { 8, 5 },
        { 10, 7 },
        { 10, 8 },

        { 6, 7 },
        { 19, 8 },
        { 15, 8 },
        { 14, 7 },
        { 17, 9 },

        { 13, 10 },
        { 13, 9 },
        { 7, 9 },
        { 6, 10 },
        { 2, 9 },

        { 5, 11 },
        { 3, 13 },
        { 1, 11 },
        { 13, 11 },
        { 13, 13 }
    };
    return positions[sensorId];
  }

  public void goUntilReachSensor(int sensorId) {
    try {
      this.start();
      if (sensorId != Constant.DONT_CARE) {
        var position = sensorIdToPosition(sensorId);
        System.out.println(this.id + " going to " + sensorId + ", ie. (" + position[0] + "," + position[1] + ")");
      }
      while (true) {
        var event = this.tsi.getSensor(this.id);
        if (event.getStatus() == SensorEvent.ACTIVE) {
          if (sensorId != Constant.DONT_CARE) {
            var position = sensorIdToPosition(sensorId);
            if (event.getXpos() == position[0] && event.getYpos() == position[1]) {
              break;
            }
          } else {
            break;
          }
        }
      }
    } catch (CommandException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    System.out.println(this.id + " reached " + sensorId);
    this.stop();
  }

  public void goUntilLeaveSensor(int sensorId) {
    try {
      this.start();
      System.out.println(this.id + " going to and leaving " + sensorId);
      while (true) {
        var event = this.tsi.getSensor(this.id);
        if (event.getStatus() == SensorEvent.INACTIVE) {
          if (sensorId != Constant.DONT_CARE) {
            var position = sensorIdToPosition(sensorId);
            if (event.getXpos() == position[0] && event.getYpos() == position[1]) {
              break;
            }
          } else {
            break;
          }
        }
      }
    } catch (CommandException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    System.out.println(this.id + " left " + sensorId);
    this.stop();
  }

  public void stopUntilAcquireTrack(int trackId) {
    this.stop();
    try {
      this.semaphores.tracks[trackId].acquire();
      System.out.println(this.id + " acquired " + trackId);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void releaseTrack(int trackId) {
    assert this.semaphores.tracks[trackId].availablePermits() == 0;
    System.out.println(this.id + " released " + trackId);
    this.semaphores.tracks[trackId].release();
  }

  public void stopForAcquireCross() {
    this.stop();
    try {
      this.semaphores.cross.acquire();
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void passCross() {
    this.stopForAcquireCross();
    this.goUntilReachSensor(Constant.DONT_CARE);
    this.semaphores.cross.release();
  }

  public void setSwitch(int s, SwitchState switchState) {
    try {
      System.out.println(this.id + " turn switch " + s + " to " + switchState);
      var state = 0;
      switch (s) {
        case 1:
          if (switchState == SwitchState.Down) {
            state = 1;
          }
          tsi.setSwitch(17, 7, state);
          break;
        case 2:
          if (switchState == SwitchState.Down) {
            state = 1;
          }
          tsi.setSwitch(15, 9, state);
          break;
        case 3:
          if (switchState == SwitchState.Up) {
            state = 1;
          }
          tsi.setSwitch(4, 9, state);
          break;
        case 4:
          if (switchState == SwitchState.Up) {
            state = 1;
          }
          tsi.setSwitch(3, 11, state);
          break;
        default:
          assert false;
      }
    } catch (CommandException e) {
      // FIXME: handle exception
    }
  }

  @Override
  public void run() {
    System.out.println(this.id + " started at " + this.currentAt + " to " + this.goingTo);
    while (true) {
      try {
        switch (this.currentAt) {
          case 0:
            assert this.goingTo == 2;
            System.out.println(this.id + " 0 -> 2");
            this.goUntilReachSensor(0);
            this.goUntilReachSensor(5);
            passCross();
            this.goUntilReachSensor(8);
            stopUntilAcquireTrack(2);
            setSwitch(1, SwitchState.Up);
            this.goUntilLeaveSensor(6);
            releaseTrack(0);
            this.currentAt = 2;
            this.goingTo = 3;
            break;
          case 1:
            assert this.goingTo == 2;
            System.out.println(this.id + " 1 -> 2");
            this.goUntilReachSensor(1);
            this.goUntilReachSensor(2);
            passCross();
            this.goUntilReachSensor(7);
            stopUntilAcquireTrack(2);
            setSwitch(1, SwitchState.Down);
            this.goUntilLeaveSensor(6);
            this.currentAt = 2;
            this.goingTo = 3;
            break;
          case 2:
            if (this.goingTo == 3) {
              System.out.println(this.id + " 2 -> 3");
              // 2 -> 3 or 4
              this.goUntilReachSensor(9);
              if (this.semaphores.tracks[3].tryAcquire()) {
                // try go to 3 first
                setSwitch(2, SwitchState.Up);
                this.goUntilReachSensor(11);
                this.releaseTrack(2);
                this.currentAt = 3;
                this.goingTo = 5;
              } else {
                System.out.println(this.id + " fallback to 2 -> 4");
                // fallback to 4
                // this.stopUntilAcquireTrack(4);
                // we don't really need semaphore for track 4
                // because if we know track 3 is hold, track 4 must be empty
                setSwitch(2, SwitchState.Down);
                this.goUntilLeaveSensor(10);
                this.releaseTrack(2);
                this.currentAt = 4;
                this.goingTo = 5;
              }
            } else if (this.goingTo == 0) {
              System.out.println(this.id + " 2 -> 0");
              this.goUntilReachSensor(6);
              if (this.semaphores.tracks[0].tryAcquire()) {
                System.out.println(this.id + " get 0");
                // try go to 0 first
                setSwitch(1, SwitchState.Up);
                this.goUntilLeaveSensor(8);
                this.releaseTrack(2);
                this.goUntilReachSensor(3);
                this.passCross();
                this.goUntilLeaveSensor(0);
                System.out.println("reached");
                this.stop();
                Thread.sleep(1000 + (50 * this.maxSpeed));
                this.start();
                this.currentAt = 0;
                this.goingTo = 2;
              } else {
                // fallback to 1
                System.out.println(this.id + " fallback to 2 -> 1");
                setSwitch(1, SwitchState.Down);
                this.goUntilLeaveSensor(7);
                this.releaseTrack(2);
                this.goUntilReachSensor(4);
                this.passCross();
                this.goUntilLeaveSensor(1);
                System.out.println("reached");
                this.stop();
                Thread.sleep(1000 + (50 * this.maxSpeed));
                this.start();
                this.currentAt = 1;
                this.goingTo = 2;
              }
            }
            break;
          case 3:
            if (this.goingTo == 5) {
              System.out.println(this.id + " 3 -> 5");
              // 3 -> 5
              this.goUntilReachSensor(12);
              this.stopUntilAcquireTrack(5);
              setSwitch(3, SwitchState.Up);
              this.goUntilLeaveSensor(14);
              this.releaseTrack(3);
              this.currentAt = 5;
              this.goingTo = 6;
            } else if (this.goingTo == 2) {
              System.out.println(this.id + " 3 -> 2");
              this.goUntilReachSensor(11);
              this.stopUntilAcquireTrack(2);
              setSwitch(2, SwitchState.Up);
              this.goUntilLeaveSensor(9);
              this.releaseTrack(3);
              this.currentAt = 2;
              this.goingTo = 0;
            } else {
              assert false;
            }
            break;
          case 4:
            if (this.goingTo == 5) {
              System.out.println(this.id + " 4 -> 5");
              this.goUntilReachSensor(13);
              this.stopUntilAcquireTrack(5);
              setSwitch(3, SwitchState.Down);
              this.goUntilLeaveSensor(14);
              // this.releaseTrack(4);
              this.currentAt = 5;
              this.goingTo = 6;
            } else if (this.goingTo == 2) {
              System.out.println(this.id + " 4 -> 2");
              this.goUntilReachSensor(10);
              this.stopUntilAcquireTrack(2);
              setSwitch(2, SwitchState.Down);
              this.goUntilLeaveSensor(9);
              // this.releaseTrack(4);
              this.currentAt = 2;
              this.goingTo = 0;
            } else {
              assert false;
            }
            break;
          case 5:
            if (this.goingTo == 6) {
              System.out.println(this.id + " 5 -> 6");
              // 5 -> 6 or 7
              this.goUntilReachSensor(17);
              if (this.semaphores.tracks[6].tryAcquire()) {
                setSwitch(4, SwitchState.Up);
                this.goUntilLeaveSensor(15);
                this.releaseTrack(5);
                this.goUntilLeaveSensor(18);
                System.out.println("reached");
                this.stop();
                Thread.sleep(1000 + (50 * this.maxSpeed));
                this.start();
                this.currentAt = 6;
                this.goingTo = 5;
              } else {
                // fallback to 7
                System.out.println(this.id + " fallback to 5 -> 7");
                setSwitch(4, SwitchState.Down);
                this.goUntilLeaveSensor(16);
                System.out.println(this.id + " reached and leave 16");
                this.releaseTrack(5);
                this.goUntilLeaveSensor(19);
                System.out.println("reached");
                this.stop();
                Thread.sleep(1000 + (50 * this.maxSpeed));
                this.start();
                this.currentAt = 7;
                this.goingTo = 5;
              }
            } else if (this.goingTo == 3) {
              System.out.println(this.id + " 5 -> 3");
              this.goUntilReachSensor(14);
              if (this.semaphores.tracks[3].tryAcquire()) {
                setSwitch(3, SwitchState.Up);
                this.goUntilLeaveSensor(12);
                this.releaseTrack(5);
                this.currentAt = 3;
                this.goingTo = 2;
              } else {
                System.out.println(this.id + " fallback to 5 -> 4");
                setSwitch(3, SwitchState.Down);
                this.goUntilLeaveSensor(13);
                this.releaseTrack(5);
                this.currentAt = 4;
                this.goingTo = 2;
              }
            } else {
              assert false;
            }
            break;
          case 6:
            assert this.goingTo == 5;
            System.out.println(this.id + " 6 -> 5");
            this.goUntilReachSensor(18);
            this.goUntilReachSensor(15);
            this.stopUntilAcquireTrack(5);
            setSwitch(4, SwitchState.Up);
            this.goUntilLeaveSensor(17);
            this.releaseTrack(6);
            this.currentAt = 5;
            this.goingTo = 3;
            break;
          case 7:
            assert this.goingTo == 5;
            System.out.println(this.id + " 7 -> 5");
            this.goUntilReachSensor(19);
            this.goUntilReachSensor(16);
            this.stopUntilAcquireTrack(5);
            setSwitch(4, SwitchState.Down);
            this.goUntilLeaveSensor(17);
            // this.releaseTrack(7);
            this.currentAt = 5;
            this.goingTo = 3;
            break;
          default:
            assert false;
        }
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } finally {
      }
    }
  }
}

public class Lab1 {
  public Lab1(int speed1, int speed2) {
    TSimInterface tsi = TSimInterface.getInstance();
    tsi.setDebug(false);
    Semaphores semaphores = new Semaphores();
    var t1 = new Train(speed1, 1, semaphores);
    t1.setSwitch(1, SwitchState.Up);
    t1.setSwitch(2, SwitchState.Up);
    t1.setSwitch(3, SwitchState.Up);
    t1.setSwitch(4, SwitchState.Up);
    var t2 = new Train(speed2, 2, semaphores);
    new Thread(t1).start();
    new Thread(t2).start();
  }
}
